#include <stdio.h>
#include <stdlib.h>

int cmpt_ope = 0;

//compter le nombre de lignes d'un fichier pour avoir sa taille 
int compt_ligne(char* nom_fichier){
	FILE* f = fopen(nom_fichier, "r"); //ouverture fichier en mode lecture "r" pour read
	int cmpt = 0;
	char ligne[20];
	while (fgets(ligne, 20, f) != NULL){
		cmpt++;
	}
	fclose(f);
	return cmpt;
}

//conversion des éléments d'un tableau en fichier
void tab_fichier(FILE* nom_fichier, int tab[], size_t taille) {

    // Vérifier si l'ouverture du fichier a réussi
    if (nom_fichier == NULL) {
        exit(EXIT_FAILURE);
    }
    // Écrire chaque élément du tableau dans le fichier
    for (size_t i = 0; i < taille; ++i) {
        fprintf(nom_fichier, "%d\n", tab[i]);
    }
    fclose(nom_fichier);
}

//conversion des éléments d'un fichier en tableaux
void fichier_tab(char* nom_fichier, int tab[]){
	//ouvrir fichier
	
	FILE* f = NULL; // declarer un fichier f et mettre le pointeur à NULL
    f = fopen(nom_fichier, "r"); //ouverture fichier en mode lecture "r" pour read
	
	if (f == NULL){
        exit(EXIT_FAILURE);
        	}
    else{ 
		
		//parcourir fichier et remplir liste 
        char ligne[20];  //taille d'une ligne
        int i = 0;
        while(fgets(ligne,20,f) != NULL) {
			tab[i] = atoi(ligne);
			i++;
		}
	}		
	fclose(f);	
}

/*===========================ENSEMBLES================================*/


//1/recherche dans ensemble NON trie
int appartient_non_trie (int x, int tab[], int taille) {
	for (int i = 0; i < taille; i++) {
		cmpt_ope++;
		if (tab[i] == x) {
			return 1;
		}
	}
	return 0;
}

//2/recherche dans ensemble trie => recherche dichotomique
int appartient_trie(int x, int tab[], int taille) {
    int debut = 0;
    int fin = taille - 1;

    while (debut <= fin) {
		
        int milieu = debut + (fin - debut) / 2;       
        cmpt_ope++;
        
        // Vérifier si l'élément est présent au milieu
        if (tab[milieu] == x) {
            // L'élément est trouvé, retourner le message et terminer la fonction
            return 1;
        }
        // Si l'élément est plus petit, ignorer la moitié droite
        if (tab[milieu] > x) {
            fin = milieu - 1;
        }
        // Si l'élément est plus grand, ignorer la moitié gauche
        else {
            debut = milieu + 1;
        }
    }
    // L'élément n'a pas été trouvé après la boucle
    return 0;
}

//3/inclusion pour ensemble NON trie
int est_inclus_non_trie(int tab1[], int taille1, int tab2[], int taille2) {
  
    // Vérifier chaque élément du tab1 dans le tab2
    for (int i = 0; i < taille1; ++i) {
		cmpt_ope++;
        if (appartient_non_trie(tab1[i], tab2, taille2) == 0) {
			
            // Si un élément n'est pas trouvé, le tab1 n'est pas inclus dans le tab2
            return 0;
        }
    }
    // Tous les éléments de l'ensembleA sont trouvés dans l'ensembleB
    return 1;
}


//4/inclusion pour ensemble trie 
int est_inclus_trie(int tab1[], int taille1, int tab2[], int taille2) {
	
    // Vérifier chaque élément du tab1 dans le tab2
    for (int i = 0; i < taille1; ++i) {
		cmpt_ope++;
        if (appartient_trie(tab1[i], tab2, taille2) == 0) {
			
            // Si un élément n'est pas trouvé, le tab1 n'est pas inclus dans le tab2
            return 0;
        }
    }
    // Tous les éléments de tab1 sont trouvés dans tab2
    return 1;
}



//5/ union pour ensemble NON trie
int* union_non_trie(int tab1[], int taille1, int tab2[], int taille2) {
    int i, j, k = 0;
    int maxTailleUnion = taille1 + taille2;

    // Allouer dynamiquement le tableau d'union
    int* tabUnion = malloc((maxTailleUnion + 1) * sizeof(int));

    // Copier les éléments de tab1 dans tabUnion
    for (i = 0; i < taille1; ++i) {
        tabUnion[k] = tab1[i];
        k++;
    }
    // Ajouter les éléments de tab2 qui ne sont pas déjà dans tabUnion pour éviter les doublons
    for (j = 0; j < taille2; j++) {	
        cmpt_ope++;	
        if (!appartient_non_trie(tab2[j], tab1, taille1)) {
            tabUnion[k] = tab2[j];
            k++;
        }	
    }

    // Enregistrez l'union dans un fichier
    FILE* UnionNonTrie = fopen("UnionNonTrie.out.txt", "w");
    tab_fichier(UnionNonTrie, tabUnion, k);

    // Retourner le tableau d'union
    return tabUnion;
}


//6/ union pour ensemble trie
int* union_trie(int tab1[], int taille1, int tab2[], int taille2) {
    int i = 0, j = 0, k = 0;
    int maxTailleUnion = taille1 + taille2;

    // Allouer dynamiquement le tableau d'union
    int* tabUnion = malloc((maxTailleUnion + 1) * sizeof(int));

    while (i < taille1 && j < taille2) {
		cmpt_ope++;
        if (tab1[i] < tab2[j]) {
            tabUnion[k] = tab1[i];
            i++;
        } 
        else if (tab1[i] > tab2[j]) {
            tabUnion[k] = tab2[j];
            j++;
            k++;
        } 
        else {
            tabUnion[k++] = tab1[i++];
            j++;
            i++;
            k++;
        }
    }
    // Ajouter les éléments restants de tab1
    while (i < taille1) {
        tabUnion[k] = tab1[i];
        k++;
        i++;
        cmpt_ope++;
    }
    // Ajouter les éléments restants de tab2
    while (j < taille2) {
        tabUnion[k] = tab2[j];
        k++;
        j++;
        cmpt_ope++;
    }

    // Enregistrez l'union dans un fichier
    FILE* UnionTrie = fopen("UnionTrie.out.txt", "w");
    tab_fichier(UnionTrie, tabUnion, k);

    // Retourner le tableau d'union
    return tabUnion;
}


//7/ intersection pour ensemble NON trie
int* intersection_non_trie(int tab1[], int taille1, int tab2[], int taille2) {
    int* tabIntersection = malloc((taille1 + taille2) * sizeof(int)); // Allouer dynamiquement le tableau d'intersection

    int k = 0; // Indice pour parcourir le tableau d'intersection

    for (int i = 0; i < taille1; ++i) {
		cmpt_ope++;
        if (appartient_non_trie(tab1[i], tab2, taille2)) {
            // Ajouter l'élément à l'intersection seulement s'il appartient aux deux ensembles
            tabIntersection[k++] = tab1[i];
        }
    }

    // Enregistrez l'intersection dans un fichier
    FILE* IntersectionNONTrie = fopen("IntersectionNONTrie.out.txt", "w");
    tab_fichier(IntersectionNONTrie, tabIntersection, k);

    // Retourner le tableau d'intersection
    return tabIntersection;
}


//8/ intersection pour ensemble trie
int* intersection_trie(int tab1[], int taille1, int tab2[], int taille2) {
    int i = 0, j = 0, k = 0;
    int maxTailleIntersection = (taille1 < taille2) ? taille1 : taille2;

    // Allouer dynamiquement le tableau d'intersection
    int* tabIntersection = malloc((maxTailleIntersection + 1) * sizeof(int));

    while (i < taille1 && j < taille2) {
		cmpt_ope++;
        if (tab1[i] < tab2[j]) {
            i++;
        } else if (tab1[i] > tab2[j]) {
            j++;
        } else {
            tabIntersection[k] = tab1[i];
            k++;
            i++;
            j++;
        }
    }

    // Enregistrez l'intersection dans un fichier
    FILE* IntersectionTrie = fopen("IntersectionTrie.out.txt", "w");
    tab_fichier(IntersectionTrie, tabIntersection, k);

    // Retourner le tableau d'intersection
    return tabIntersection;
}


/*==================================PROGRAMME_PRINCIPAL=================================*/

int main(int argc,char** argv){
	char* f1;
	char* f2;
	
	if (argc >= 2) {
		f1 = argv[1];
		f2 = argv[2];
	}
	else return 0;
	
	int taille1=compt_ligne(f1);
	int taille2=compt_ligne(f2);
	
	int tab1[taille1];
	int tab2[taille2];
	
	fichier_tab(f1, tab1);
	fichier_tab(f2, tab2);

	int choix;
	puts("\n\n\t~~~~~~ M E N U ~~~~~~\n");
	puts("\t1 : Recherchr un entier dans un ensemble NON trie (le premier fichier entré)");
	puts("\t2 : Recherchr un entier dans un ensemble trie(le premier fichier entré)");
	puts("\t3 : Verifier si un ensemble est inclu dans un autre cas NON trie");
	puts("\t4 : Verifier si un ensemble est inclu dans un autre cas trie");
	puts("\t5 : Calcul de l'union de deux ensembles cas NON trie");
	puts("\t6 : Calcul de l'union de deux ensembles cas trie");
	puts("\t7 : Calcul de l'intersection de deux ensembles cas NON trie");
	puts("\t8 : Calcul de l'intersection de deux ensembles cas trie");
	puts("\t9 : Quitter le programme");
			
	do {
		printf("\t\tVotre choix : ");
        scanf("%d", &choix);
        printf("\n");
	} while (choix < 1 || choix > 9);

	switch (choix) {
		case 1:
			int elt1, res;
			printf("Votre choix d'élément à chercher : ");
			scanf("%d", &elt1);
			res = appartient_non_trie(elt1, tab1, taille1);
			if (res == 1) {
				printf("L'élément %d est inclu dans le fichier \n", elt1); 
			} 
			else {
				printf("L'élément %d n'est pas inclu dans le fichier \n", elt1); 
			}
			printf("Compteur: %d \n", cmpt_ope); 
            break;

		case 2:
			int elt2, res2;
			printf("Votre choix d'élément à chercher : ");
			scanf("%d", &elt2);
            res2 = appartient_trie(elt2, tab1, taille1);
            if (res2 == 1) {
				printf("L'élément %d est inclu dans le fichier \n", elt2); 
			} 
			else {
				printf("L'élément %d n'est pas inclu dans le fichier \n", elt2); 
			}
			printf("Compteur: %d \n", cmpt_ope); 
            break;

		case 3:
			int res3;
			res3 = est_inclus_non_trie(tab1, taille1, tab2, taille2);
			if (res3 == 0) {
				printf("Le premier ensemble n'appartient pas au second \n");
			} 
			else {
				printf("Le premier ensemble appartient au second \n"); 
			}
			printf("Compteur: %d \n", cmpt_ope); 
            break;

        case 4:
			int res4;
            res4 = est_inclus_trie(tab1, taille1, tab2, taille2);
            if (res4 == 0) {
				printf("Le premier ensemble n'appartient pas au second \n");
			} 
			else {
				printf("Le premier ensemble appartient au second \n"); 
			}
			printf("Compteur: %d \n", cmpt_ope); 
            break;

        case 5:
			union_non_trie(tab1, taille1, tab2, taille2);
			printf("Compteur: %d \n", cmpt_ope); 
            break;

        case 6:
			union_trie(tab1, taille1, tab2, taille2);
			printf("Compteur: %d \n", cmpt_ope); 
            break;

        case 7:
			intersection_non_trie(tab1, taille1, tab2, taille2);
			printf("Compteur: %d \n", cmpt_ope); 
            break;

        case 8:
			intersection_trie(tab1, taille1, tab2, taille2);
			printf("Compteur: %d \n", cmpt_ope); 
            break;
                
        case 9:
			break;
	}
	return 0;
}
	
	
	
	
	
