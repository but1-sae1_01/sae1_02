/* ---------------------------------------------------------------------------
 * Génération de fichiers d'entiers de manière aléatoire, triés et non triés
 * Auteur     : Damien Genthial
 * Entrées    : sur la ligne de commande, nb : nombre d'entiers et nom : nom de base du fichier
 * Sorties    : deux fichiers nom.nb.txt et nom.nb.trie.txt contenant nb entiers aléatoires
 *              dans 0..MAXINT-1
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <linux/limits.h>

const int MAXINT = 32768;
int cmp(const void* x, const void* y);
bool estDans(int t[], int nb, int x);

int main(int nbArgs, char* arg[])
{
    if (nbArgs != 3) {
        fprintf(stderr, "Usage :\n\t genereFichier nbEntiers nomFichiers\n");
        fprintf(stderr, "\t\tproduit nomFichiers.txt et nomFichiers-trie.txt\n");
        fprintf(stderr, "\t\tcontenant nbEntiers entre 0 et %d\n", MAXINT);
    }

    srand(time(NULL));

    int nb = atoi(arg[1]);
    // Construction de l'ensemble (sans répétition...)
    int* tab = (int*) malloc(nb * sizeof(int));
    int x;
    int i = 0;
    while (i < nb) {
       x =  rand() % MAXINT;
       if (! estDans(tab, nb, x)) {
           tab[i] = x;
           i++;
       }
    }

    // Création du fichier non trié
    char nomF[PATH_MAX];
    sprintf(nomF, "%s.%d.txt", arg[2], nb);
    FILE* f = fopen(nomF, "w");
    for (int i = 0; i < nb; i++) {
        fprintf(f, "%d\n", tab[i]);
    }
    fclose(f);

    // Tri de la table
    qsort(tab, nb, sizeof(int), cmp);

    // Création du fichier trié
    sprintf(nomF, "%s.%d.trie.txt", arg[2], nb);
    f = fopen(nomF, "w");
    for (int i = 0; i < nb; i++) {
        fprintf(f, "%d\n", tab[i]);
    }
    fclose(f);
    return EXIT_SUCCESS;
}

int cmp(const void* x, const void* y) {
    int a = *((int*) x);
    int b = *((int*) y);
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
}

bool estDans(int t[], int nb, int x) {
    int i = 0;
    while ((i< nb) && (t[i] != x))
        i++;
    return (i < nb);
}
